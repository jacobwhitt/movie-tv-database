import React, { useEffect, useState } from "react";
import {
  ActivityIndicator,
  Dimensions,
  StyleSheet,
  BackHandler,
  SafeAreaView,
  StatusBar,
} from "react-native";
import * as Font from "expo-font";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import Navigation from "./views/Navigation";

const Stack = createStackNavigator();
const { width, height } = Dimensions.get("window");

export default function App() {
  let [fontsLoaded, setFontsLoaded] = useState(false);

  StatusBar.setBarStyle("inverted", true);

  const loadFonts = async () => {
    await Font.loadAsync({
      Avenir: require("./assets/AvenirNextLTPro-Regular.otf"),
    }).then(() => setFontsLoaded(true));
  };

  useEffect(() => {
    loadFonts();

    const backAction = () => {
      Alert.alert("Wait! Are you sure you wanna leave?", [
        {
          text: "Whoa! No way, this is too much fun!",
          onPress: () => null,
          style: "cancel",
        },
        { text: "Get me outta here!", onPress: () => BackHandler.exitApp() },
      ]);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );

    return () => backHandler.remove();
  }, []);

  if (!fontsLoaded) {
    return <ActivityIndicator />;
  } else {
    return (
      <SafeAreaView style={styles.container}>
        <StatusBar barStyle="light-content" />
        <NavigationContainer>
          <Stack.Navigator
            screenOptions={{
              headerShown: false,
              gestureEnabled: false,
            }}
          >
            <Stack.Screen name="Navigation">
              {(props) => <Navigation {...props} />}
            </Stack.Screen>
          </Stack.Navigator>
        </NavigationContainer>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: width,
    height: height,
    backgroundColor: "black",
  },
});
