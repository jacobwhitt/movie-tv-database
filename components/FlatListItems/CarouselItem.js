import React, { useEffect } from "react";
import { StyleSheet, Text, TouchableOpacity, Image, View } from "react-native";
import setPoster from "../utils/Poster";
import { genres } from "../utils/genresList";

const CarouselItem = ({ item, onPress, style }) => {
  useEffect(() => {
    item.genre_ids.forEach((genreID, index) => {
      genres.forEach((genre) => {
        if (genreID === genre.id) {
          item.genre_ids[index] = genre.name + " ";
        }
      });
    });
  }, []);

  const wordWrap = (string, maxWidth) => {
    const newLineString = "\n";
    let res = "";

    while (string.length > maxWidth) {
      let found = false;

      // Inserts new line at first whitespace of the line
      for (let i = maxWidth - 1; i >= 0; i--) {
        if (findWhiteSpace(string.charAt(i))) {
          res = res + [string.slice(0, i), newLineString].join("");
          string = string.slice(i + 1);
          found = true;
          break;
        }
      }

      // Inserts new line at maxWidth position, the word is too long to wrap
      if (!found) {
        res += [string.slice(0, maxWidth), newLineString].join("");
        string = string.slice(maxWidth);
      }
    }

    return res + string;
  };

  const findWhiteSpace = (x) => {
    const whiteSpace = new RegExp(/^\s$/);
    return whiteSpace.test(x.charAt(0));
  };

  return (
    <TouchableOpacity onPress={onPress} style={[styles.item, style]}>
      <View style={styles.itemFlex}>
        <Image source={{ uri: setPoster(item) }} style={styles.image} />
        <View style={styles.itemInfo}>
          <Text style={styles.title}>{item.title ? wordWrap(item.title, 25) : wordWrap(item.name, 25)}</Text>
          <Text style={styles.text}>{item.genre_ids.length > 2 ? wordWrap(item.genre_ids[0] + item.genre_ids[1], 20) : wordWrap(item.genre_ids, 20)}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default CarouselItem;

const styles = StyleSheet.create({
  image: {
    width: 100,
    height: 150,
    resizeMode: "center",
  },
  item: {
    marginRight: 5,
    padding: 15,
    shadowOpacity: 0.7,
    shadowColor: "#000",
    shadowOffset: { width: 4, height: 4 },
    // backgroundColor: "green",
  },
  itemFlex: {
    width: 200,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
  },
  itemInfo: {
    marginTop: 10,
  },
  title: {
    color: "#000",
    fontSize: 16,
    fontWeight: "bold",
  },
  text: {
    color: "#999",
    fontSize: 12,
    fontWeight: "bold",
  },
});
