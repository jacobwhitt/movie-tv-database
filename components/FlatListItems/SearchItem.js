import React from "react";
import { StyleSheet, Text, TouchableOpacity, Image, View } from "react-native";
import setPoster from "../utils/Poster";

const SearchItem = ({ item, onPress, style }) => {
  return (
    <TouchableOpacity onPress={onPress} style={[styles.item, style]}>
      <View style={styles.itemFlex}>
        <Image source={{ uri: setPoster(item) }} style={styles.image} />

        <View style={styles.itemInfo}>
          <Text style={styles.title}>{item.title}</Text>
          <Text style={styles.description}>
            {item.overview.substring(0, 120)}...
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default SearchItem;

const styles = StyleSheet.create({
  description: {
    width: 180,
    fontSize: 16,
    flexWrap: "wrap",
    color: "#333",
    marginBottom: 5,
  },
  icon: {
    marginRight: 5,
  },
  image: {
    width: 120,
    height: 120,
    resizeMode: "contain",
  },
  item: {
    marginVertical: 5,
    padding: 5,
    alignItems: "center",
    width: 320,
    shadowOpacity: 0.7,
    shadowColor: "#000",
    shadowOffset: { width: 3, height: 3 },
  },
  itemFlex: {
    // backgroundColor: "#00AA5088",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    width: 300,
  },
  itemInfo: {
    color: "#fff",
    height: "100%",
    width: 200,
  },
  title: {
    color: "#000",
    fontSize: 20,
    fontWeight: "bold",
  },
});
