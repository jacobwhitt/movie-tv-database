const axios = require("axios");
const { APIKEY, TMDBURL } = require("../config");

/// TMDB API REQUESTS ///
const tmdbRequests = {
  // TOP 4 USED FOR DASHBOARD CAROUSELS
  getPopMovies: async function () {
    try {
      const response = await axios.get(`${TMDBURL}/trending/movie/week?api_key=${APIKEY}`);
      if (response.statusText === "OK" || response.status === 200) {
        const sortArr = [];
        sortArr.push(...response.data.results);
        sortArr.sort(function (a, b) {
          return b.popularity - a.popularity;
        });
        return sortArr;
      }
    } catch (err) {
      if (err.response) {
        console.log(err.response);
      } else {
        console.log(err);
      }
    }
  },

  getPopTV: async function () {
    try {
      const response = await axios.get(`${TMDBURL}/trending/tv/week?api_key=${APIKEY}`);
      if (response.statusText === "OK" || response.status === 200) {
        const sortArr = [];
        sortArr.push(...response.data.results);
        sortArr.sort(function (a, b) {
          return b.popularity - a.popularity;
        });
        return sortArr;
      }
    } catch (err) {
      if (err.response) {
        console.log(err.response);
      } else {
        console.log(err);
      }
    }
  },

  getFamilyMovies: async function (query, page = 1) {
    try {
      const response = await axios.get(`${TMDBURL}/discover/movie?api_key=${APIKEY}&with_genres=10751`);
      const familyArr = [];

      if (response.statusText === "OK" || response.status === 200) {
        response.data.results.forEach((movie) => {
          if (movie.genre_ids.includes(10751)) {
            familyArr.push(movie);
            familyArr.sort(function (a, b) {
              return b.popularity - a.popularity;
            });
          }
        });
      }
      return familyArr;
    } catch (err) {
      if (err.response) {
        console.log(err.response);
      } else {
        console.log(err);
      }
    }
  },

  getDocumentaries: async function () {
    try {
      const response = await axios.get(`${TMDBURL}/discover/movie?api_key=${APIKEY}&with_genres=99&without_keywords=`);
      const docArr = [];

      if (response.statusText === "OK" || response.status === 200) {
        response.data.results.forEach((movie) => {
          if (movie.title.toLowerCase().includes("porn") || movie.overview.includes("porn")) {
            return;
          }
          if (movie.genre_ids.includes(99)) {
            docArr.push(movie);
          } else {
            return;
          }
        });
      }
      return docArr;
    } catch (err) {
      if (err.response) {
        console.log(err.response);
      } else {
        console.log(err);
      }
    }
  },

  // used for general movie search
  getMovies: async function (query, page = 1) {
    try {
      const response = await axios.get(`${TMDBURL}/search/movie?api_key=${APIKEY}&language=en-US&query=${query}&page=${page}&include_adult=false`);
      if (response.statusText === "OK" || response.status === 200) {
        return response.data.results;
      }
    } catch (err) {
      if (err.response) {
        console.log(err.response);
      } else {
        console.log(err);
      }
    }
  },

  // used for details component
  getMovieDetails: async function (id) {
    try {
      const response = await axios.get(`${TMDBURL}/movie/${id}?api_key=${APIKEY}&language=en-US`);
      if (response.statusText === "OK" || response.status === 200) {
        return response.data;
      }
    } catch (err) {
      if (err.response) {
        console.log(err.response);
      } else {
        console.log(err);
      }
    }
  },

  getTVDetails: async function (id) {
    try {
      const response = await axios.get(`${TMDBURL}/tv/${id}?api_key=${APIKEY}&language=en-US`);
      if (response.statusText === "OK" || response.status === 200) {
        return response.data;
      }
    } catch (err) {
      if (err.response) {
        console.log(err.response);
      } else {
        console.log(err);
      }
    }
  },

  // getMovieGenres: async function () {
  //   try {
  //     const response = await axios.get(`${TMDBURL}/genre/movie/list?api_key=${APIKEY}&language=en-US`);
  //     if (response.statusText === "OK" || response.status === 200) {
  //       console.log("MOVIE GENRES: ", response.data);
  //     }
  //   } catch (err) {
  //     if (err.response) {
  //       console.log(err.response);
  //     } else {
  //       console.log(err);
  //     }
  //   }
  // },

  // getTvGenres: async function () {
  //   try {
  //     const response = await axios.get(`${TMDBURL}/genre/tv/list?api_key=${APIKEY}&language=en-US`);
  //     if (response.statusText === "OK" || response.status === 200) {
  //       console.log("TV GENRES: ", response.data);
  //     }
  //   } catch (err) {
  //     if (err.response) {
  //       console.log(err.response);
  //     } else {
  //       console.log(err);
  //     }
  //   }
  // },
};

module.exports = tmdbRequests;
