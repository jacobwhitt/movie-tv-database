// Reuseable algorithm to set posters across all components via import

const setPoster = (item) => {
  return item.poster_path === null ? "https://image.flaticon.com/icons/png/512/1687/1687282.png" : `http://image.tmdb.org/t/p/w342${item.poster_path}`;
};

export default setPoster;
