import React from "react";
import Icon from "react-native-vector-icons/FontAwesome";
import makeID from "./makeID";

// Reuseable algorithm to set filled/greyed out stars for movie/series ratings across all components via import

const setStars = (item) => {
  let ratingArr = [];
  let rating = Math.round((item.vote_average / 20) * 10);
  if (rating < 5) {
    let gray = 5 - rating;
    for (let i = 0; i < rating; i++) {
      ratingArr.push(<Icon name='star' size={20} color='gold' key={makeID(4)} />);
    }
    for (let g = 0; g < gray; g++) {
      ratingArr.push(<Icon name='star' size={20} color='slategray' key={makeID(4)} />);
    }
  }
  return ratingArr;
};

export default setStars;
