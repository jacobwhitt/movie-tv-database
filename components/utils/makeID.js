// make unique IDs for flatlist children and the ratings stars (anything that gets mapped basically as React Native requires mapped children elements to have unique ideas for efficiency and/or indexing)
const makeID = (length) => {
  let result = [];
  let characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  let charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result.push(characters.charAt(Math.floor(Math.random() * charactersLength)));
  }
  return result.join("");
};

export default makeID