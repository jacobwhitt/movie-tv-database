const APIKEY = "4e08a4e5895c922762a3edbf8d3d9b89";
const TMDBURL = "https://api.themoviedb.org/3";

module.exports = {
  APIKEY,
  TMDBURL,
};
