import React, { useEffect, useState } from "react";
import {
  ActivityIndicator,
  Alert,
  Dimensions,
  Image,
  Keyboard,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from "react-native";
import {
  FlatList,
  TextInput,
  TouchableOpacity,
} from "react-native-gesture-handler";

import Icon from "react-native-vector-icons/FontAwesome";
import makeID from "../components/utils/makeID";
import CarouselItem from "../components/FlatListItems/CarouselItem";
import tmdbRequests from "../components/myAPI";

const { width, height } = Dimensions.get("window");

const Dashboard = (props) => {
  const [searchInput, setSearchInput] = useState("");
  const [loading, setLoading] = useState(false);

  // fetches of movie data for carousels rendering are stored in the state
  const [popMovies, setPopMovies] = useState([]);
  const [popTV, setPopTV] = useState([]);
  const [familyMovies, setFamilyMovies] = useState([]);
  const [documentaries, setDocumentaries] = useState([]);

  useEffect(() => {
    tmdbRequests.getFamilyMovies().then((response) => {
      setFamilyMovies(response);
    });

    tmdbRequests.getDocumentaries().then((response) => {
      setDocumentaries(response);
    });

    tmdbRequests.getPopMovies().then((response) => {
      setPopMovies(response);
    });

    tmdbRequests.getPopTV().then((response) => {
      setPopTV(response);
    });
  }, []);

  const handleUserInputChange = (text) => {
    if (/^[a-zA-Z0-9\s]+$/.test(text) || text === "") {
      console.log("Movie:", text);
      setSearchInput(text);
    }
    setSearchInput(text);
  }

  const handleSearch = () => {
    setLoading(true);
    tmdbRequests.getMovies(searchInput).then((response) => {
      setTimeout(() => {
        setLoading(false);
        props.navigation.navigate("Search", {
          clear: clearSearchInput(),
          movieList: response,
          search: searchInput,
        });
      }, 1000);
    });
  };

  const clearSearchInput = () => {
    setSearchInput("");
  };

  // Renders the carousels on the Dashboard using the CarouselItem FlatList component
  const renderCarouselItems = ({ item }) => {
    return (
      <CarouselItem
        item={item}
        onPress={() => {
          setLoading(true);
          let type;
          switch (item.media_type === "tv" ? (type = "tv") : (type = "movie")) {
            case "tv":
              tmdbRequests.getTVDetails(item.id).then((response) => {
                setTimeout(() => {
                  setLoading(false);
                  props.navigation.navigate("Details", { serie: response });
                }, 500);
              });
              break;
            case "movie":
              tmdbRequests.getMovieDetails(item.id).then((response) => {
                setTimeout(() => {
                  setLoading(false);
                  props.navigation.navigate("Details", { movie: response });
                }, 500);
              });
              break;
          }
        }}
        style={[styles.carouselItems, {}]}
      />
    );
  };

  return (
    <View>
      {loading ? (
        <ActivityIndicator
          size="large"
          color="#48BBEC"
          style={{
            position: "absolute",
            top: height * 0.45,
            right: width * 0.45,
          }}
        />
      ) : (
        <View style={styles.container} onPress={() => Keyboard.dismiss()}>
          <View style={styles.searchBar}>
            {/* SEARCH BAR TEXT INPUT */}
            <View style={styles.input}>
              <TextInput
                value={searchInput}
                onChangeText={handleUserInputChange}
                multiline={false}
                onSubmitEditing={() => handleSearch()}
                placeholder="Search..."
                placeholderTextColor="#000"
                style={styles.inputText}
              />

              {/* CLEAR INPUT X BUTTON */}
              {searchInput ? (
                <TouchableOpacity
                  style={styles.clearButton}
                  onPress={() => {
                    handleSearch();
                  }}
                >
                  <Icon name="times-circle" size={20} color="#ccc" />
                </TouchableOpacity>
              ) : null}
            </View>

            {/* SEARCH BUTTON ICON */}
            <TouchableOpacity
              style={styles.inputButton}
              onPress={() => {
                if (!searchInput) {
                  Alert.alert(
                    "Missing Input",
                    "Please enter alphanumeric text in the search bar!"
                  );
                } else {
                  Keyboard.dismiss();
                  setLoading(true);
                  handleSearch();
                }
              }}
            >
              <Icon name="search" size={30} color="#fb0046" />
            </TouchableOpacity>
          </View>

          <ScrollView style={styles.carouselContainer}>
            <View style={{ paddingBottom: 80 }}>
              {/* SPLASH IMAGE */}
              <View style={styles.splashContainer}>
                <Text style={styles.dashTitle}>
                  Popular movies and TV series at your fingertips!
                </Text>
                <Image
                  style={styles.splashImage}
                  source={require("../assets/splashImage.png")}
                />
              </View>

              {/* POPULAR MOVIES CAROUSEL */}
              <View style={styles.carousel1}>
                <Text style={styles.title}>Popular Movies This Week</Text>
                <Text style={styles.text}>
                  Discover top rated movies of the week
                </Text>

                <FlatList
                  contentContainerStyle={styles.flatlist}
                  data={popMovies}
                  renderItem={renderCarouselItems}
                  keyExtractor={(item) => makeID(8)}
                  horizontal={true}
                  numColumns={1}
                  initialScrollIndex={0}
                />
              </View>

              {/* POPULAR TV CAROUSEL */}
              <View style={styles.carousel2}>
                <Text style={styles.title}>Popular Series This Week</Text>
                <Text style={styles.text}>Keep up with trending TV series</Text>
                <FlatList
                  contentContainerStyle={styles.flatlist}
                  data={popTV}
                  renderItem={renderCarouselItems}
                  keyExtractor={(item) => makeID(8)}
                  horizontal={true}
                  numColumns={1}
                  initialScrollIndex={0}
                />
              </View>

              {/* FAMILY MOVIES CAROUSEL */}
              <View style={styles.carousel1}>
                <Text style={styles.title}>Family Movies</Text>
                <Text style={styles.text}>
                  Experiences the entire family with love
                </Text>
                <FlatList
                  contentContainerStyle={styles.flatlist}
                  data={familyMovies}
                  renderItem={renderCarouselItems}
                  keyExtractor={(item) => makeID(8)}
                  horizontal={true}
                  numColumns={1}
                  initialScrollIndex={0}
                />
              </View>

              {/* DOCUMENTARIES CAROUSEL */}
              <View style={styles.carousel2}>
                <Text style={styles.title}>Documentaries</Text>
                <Text style={styles.text}>
                  Broaden your horizons with global documentaries
                </Text>
                <FlatList
                  contentContainerStyle={styles.flatlist}
                  data={documentaries}
                  renderItem={renderCarouselItems}
                  keyExtractor={(item) => makeID(8)}
                  horizontal={true}
                  numColumns={1}
                  initialScrollIndex={0}
                />
              </View>
            </View>
          </ScrollView>
        </View>
      )}
    </View>
  );
};

export default Dashboard;

const styles = StyleSheet.create({
  container: {
    width: width,
    height: height,
    // backgroundColor: "#582860",
  },
  btnText: {
    fontSize: 20,
    color: "#000",
    textAlign: "center",
    fontStyle: "italic",
  },
  carouselContainer: {},
  carousel1: {
    backgroundColor: "#1e778533",
  },
  carousel2: {
    backgroundColor: "#58286033",
  },
  carouselItems: {
    height: 250,
  },
  clearButton: {
    marginLeft: 20,
  },
  dashTitle: {
    color: "#000",
    fontSize: 30,
    fontFamily: "Avenir",
    textAlign: "center",
  },
  input: {
    flexDirection: "row",
    alignItems: "center",
    width: "80%",
    marginTop: 10,
    marginLeft: 10,
    borderColor: "#ccc",
    borderWidth: 1,
    borderRadius: 5,
  },
  inputText: {
    color: "#000",
    width: "85%",
    height: 36,
    padding: 10,
    fontSize: 14,
  },
  inputButton: {
    marginTop: 5,
  },
  searchBar: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-evenly",
  },
  splashContainer: {
    width: "100%",
    alignItems: "center",
    marginTop: 10,
  },
  splashImage: {
    width: "100%",
    height: 200,
    resizeMode: "contain",
  },
  subtitle: {
    color: "#000",
    fontSize: 24,
    marginLeft: 20,
    fontFamily: "Avenir",
  },
  title: {
    color: "#000",
    fontSize: 30,
    marginTop: 20,
    marginLeft: 20,
    fontFamily: "Avenir",
  },
  text: {
    color: "#000",
    fontSize: 18,
    marginLeft: 20,
    fontFamily: "Avenir",
  },
});
