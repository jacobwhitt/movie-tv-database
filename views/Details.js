import React, { useState } from "react";
import { Dimensions, Image, Linking, Modal, Pressable, ScrollView, StyleSheet, Text, View } from "react-native";

import setPoster from "../components/utils/Poster";
import setStars from "../components/utils/Ratings";
import Icon from "react-native-vector-icons/FontAwesome";

const { width, height } = Dimensions.get("window");

const Details = (props) => {
  const [modal, setModal] = useState(false);

  let details, type;
  props.route.params.movie ? ((details = props.route.params.movie), (type = "movie")) : ((details = props.route.params.serie), (type = "serie"));
  // console.log("Details of item", details);

  const getGenres = details.genres.map((genre) => {
    return genre.name + ", ";
  });

  return (
    <View style={styles.container}>
      {/* MODAL FOR SELECTING VIDEO PREVIEW TYPES */}
      <Modal animationType='slide' transparent={true} visible={modal}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <>
              {/* OFFICIAL HOMEPAGE LINK */}
              {details.homepage ? (
                <Pressable
                  style={({ pressed }) => [{ backgroundColor: pressed ? "#28242c55" : "#28242c" }, styles.modalBtn]}
                  onPress={() => {
                    Linking.openURL(`${details.homepage}`);
                  }}>
                  <Text style={styles.btnText}>Official Page</Text>
                </Pressable>
              ) : null}

              {/* LINK FOR MORE INFO ON IMDb */}
              {details.imdb_id ? (
                <Pressable
                  style={({ pressed }) => [{ backgroundColor: pressed ? "#28242c55" : "#28242c" }, styles.modalBtn]}
                  onPress={() => {
                    Linking.openURL(`https://www.imdb.com/title/${details.imdb_id}`);
                  }}>
                  <Text style={styles.btnText}>IMDb</Text>
                </Pressable>
              ) : null}

              {/* CLOSE "PREVIEWS" MODAL */}
              <Pressable
                style={({ pressed }) => [{ backgroundColor: pressed ? "#FF000055" : "#FF0000" }, styles.modalBtn]}
                onPress={() => {
                  setModal(!modal);
                }}>
                <Text style={styles.btnText}>Close</Text>
              </Pressable>
            </>
          </View>
        </View>
      </Modal>

      {/* NAVIGATION TO PREVIOUS PAGE/COMPONENT */}
      <Pressable
        style={({ pressed }) => [{ opacity: pressed ? 0.3 : 1 }, styles.back]}
        onPress={() => {
          props.navigation.goBack();
        }}>
        <Icon style={{ color: "white" }} name='arrow-left' size={20} color='white' />
        <Text style={styles.btnText}>Back</Text>
      </Pressable>

      {/* MOVIE/SERIE POSTER PATH IMAGE */}
      <Image source={{ uri: setPoster(details) }} style={styles.image} />

      {/* STYLING THE DETAILS VIEW TEXT */}
      <View style={styles.movieInfo}>
        {/* OPENS A MODAL TO EXTERNAL LINKS */}
        <Pressable
          style={({ pressed }) => [{ opacity: pressed ? 0.3 : 1 }, styles.pressable]}
          hitSlop={40}
          pressRetentionOffset={{ bottom: 20, left: 20, right: 20, top: 20 }}
          onPress={() => {
            setModal(true);
          }}>
          <Text style={styles.btnText}>Links</Text>
        </Pressable>

        <Text style={[styles.title, { fontWeight: "bold" }]}>{type === "movie" ? details.title : details.name}</Text>

        <ScrollView contentContainerStyle={styles.scrollView} snapToEnd='yes'>
          {/* RATING */}
          <Text style={styles.text}>
            {setStars(details)} ({details.vote_count} votes)
          </Text>

          {/* RUNTIME */}
          <Text style={styles.text}>
            <Text style={{ fontWeight: "bold" }}>Runtime:</Text> {details.runtime} minutes
          </Text>

          {/* PLOT */}
          <Text style={styles.text}>
            <Text style={{ fontWeight: "bold" }}>Plot:</Text> {details.overview}
          </Text>

          {/* RELEASE DATE */}
          <Text style={styles.text}>
            {type === "movie" ? (
              <Text>
                <Text style={{ fontWeight: "bold" }}>Released:</Text> {details.release_date}
              </Text>
            ) : (
              <Text>
                <Text style={{ fontWeight: "bold" }}>First aired:</Text> + {details.seasons[0].air_date}
              </Text>
            )}
          </Text>

          {/* GENRES */}
          <Text style={styles.text}>
            <Text style={{ fontWeight: "bold" }}>Genres:</Text> {getGenres}
          </Text>
        </ScrollView>
      </View>
    </View>
  );
};

export default Details;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: width,
    height: height,
    alignItems: "center",
    // backgroundColor: "#582860",
  },
  back: {
    position: "absolute",
    left: 10,
    top: 10,
    width: 80,
    height: 40,
    backgroundColor: "rgba(100, 100, 100, 0.8)",
    borderRadius: 15,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-evenly",
    zIndex: 2,
  },
  btnText: {
    fontSize: 24,
    color: "#fff",
    fontStyle: "italic",
    fontFamily: "Avenir",
  },
  centeredView: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  image: {
    width: "100%",
    height: "50%",
    resizeMode: "contain",
    // backgroundColor: "#000",
  },
  modalBtn: {
    width: 180,
    borderRadius: 10,
    marginTop: 10,
    padding: 10,
    shadowColor: "#ccc",
    shadowOffset: {
      width: 1,
      height: 2,
    },
    shadowOpacity: 0.5,
    shadowRadius: 1,
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
  modalView: {
    backgroundColor: "rgba(10, 10, 10, 0.5)",
    borderRadius: 10,
    padding: 35,
    alignItems: "center",
    shadowColor: "#fff",
    shadowOffset: {
      width: 3,
      height: 4,
    },
    shadowOpacity: 0.9,
    shadowRadius: 3,
  },
  movieInfo: {
    width: "100%",
    height: "100%",
    padding: 10,
    // backgroundColor: "rgba(10, 50, 10, 0.8)",
    fontFamily: "Avenir",
  },
  pressable: {
    position: "absolute",
    top: 5,
    right: 15,
    width: 80,
    height: 40,
    zIndex: 2,
    flexDirection: "row",
    borderRadius: 15,
    backgroundColor: "rgba(10, 10, 10, 0.3)",
    alignItems: "center",
    justifyContent: "center",
  },
  scrollView: {
    height: 800,
  },
  title: {
    color: "#000",
    marginTop: 20,
    fontSize: 18,
    fontFamily: "Avenir",
  },
  text: {
    color: "#000",
    marginTop: 5,
    fontSize: 16,
    fontFamily: "Avenir",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
    fontFamily: "Avenir",
  },
});
