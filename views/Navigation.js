import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { Dimensions, StyleSheet } from "react-native";
const Stack = createStackNavigator();
const { width, height } = Dimensions.get("window");

import Dashboard from "./Dashboard";
import Details from "./Details";
import Search from "./Search";

const Navigation = (props) => {
  return (
    <Stack.Navigator
      headerMode="none"
      screenOptions={{
        headerShown: false,
        gestureEnabled: true,
      }}
    >
      <Stack.Screen name="Dashboard">
        {(props) => <Dashboard {...props} />}
      </Stack.Screen>

      <Stack.Screen name="Details">
        {(props) => <Details {...props} />}
      </Stack.Screen>

      <Stack.Screen name="Search">
        {(props) => <Search {...props} />}
      </Stack.Screen>
    </Stack.Navigator>
  );
};
export default Navigation;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    width: width,
    height: height,
  },
});
