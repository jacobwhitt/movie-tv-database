import React, { useRef, useState, useEffect } from "react";
import { ActivityIndicator, Dimensions, FlatList, Pressable, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";

import SearchItem from "../components/FlatListItems/SearchItem";
import makeID from "../components/utils/makeID";
import tmdbRequests from "../components/myAPI";
const { width, height } = Dimensions.get("window");
const myFont = "Avenir";

const Search = (props) => {
  // Control displayed list of movies and OFFSET the page number of the API call to get the next page of results
  const [datasource, setDataSource] = useState([]);
  const [offset, setOffset] = useState(1);
  const [footerLoading, setFooterLoading] = useState(false);

  // Control navigation to individual movie details
  const [scrolled, setScrolled] = useState(false);
  const [selectedId, setSelectedId] = useState(null);
  const [index, setIndex] = useState(0);
  const [loading, setLoading] = useState(false);

  // Set movies found to the state
  useEffect(() => setDataSource(props.route.params.movieList), []);
  let search = props.route.params.search;
  // console.log(datasource);

  // Control appearance of "scroll-to-top" button at top of screen
  const onViewableItemsChanged = ({ viewableItems }) => {
    viewableItems[0].index === 0 ? setScrolled(false) : setScrolled(true);
  };
  const viewabilityConfigCallbackPairs = useRef([{ onViewableItemsChanged }]);

  // Renders each item in the <FlatList> using the <Item> component from the components folder
  const renderItem = ({ item, index }) => {
    const selectedShadow = item.id === selectedId ? "#fb0046" : "slategray";
    const selectedBorder = item.id === selectedId ? 2 : 0;

    return (
      <SearchItem
        item={item}
        onPress={() => {
          setSelectedId(item.id);
          setIndex(index);
          setLoading(true);
          tmdbRequests.getMovieDetails(item.id).then((response) => {
            setTimeout(() => {
              setLoading(false);
              props.navigation.navigate("Details", { movie: response });
            }, 500);
          });
        }}
        style={[
          styles.item,
          { borderColor: selectedShadow, borderWidth: selectedBorder },
        ]}
      />
    );
  };

  // Renders the footer within the <FlatList> that holds the button to load more results
  const renderFooter = () => {
    if (datasource.length === 0) {
      return null;
    } else {
      return (
        //Footer View with Load More button
        <View style={styles.footer}>
          {footerLoading ? (
            <ActivityIndicator
              color="#000"
              style={{ marginLeft: 8, alignItems: "center" }}
            />
          ) : (
            <TouchableOpacity
              activeOpacity={0.9}
              onPress={() => {
                setFooterLoading(true);
                tmdbRequests.getMovies(search, offset + 1).then((response) => {
                  setDataSource([...datasource, ...response]);
                  setTimeout(() => {
                    setOffset(offset + 1);
                    setFooterLoading(false);
                  }, 500);
                });
              }}
              style={styles.loadMoreBtn}
            >
              <Text style={styles.btnText}>Load More</Text>
            </TouchableOpacity>
          )}
        </View>
      );
    }
  };

  // Scroll-to-top references/Fn
  const flatlistRef = useRef();
  const scrollToTop = () => {
    flatlistRef.current.scrollToIndex({ index: 0 });
  };

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Pressable
          style={({ pressed }) => [styles.back, { opacity: pressed ? 0.3 : 1 }]}
          onPress={() => {
            props.navigation.navigate("Dashboard");
          }}
        >
          <Icon
            style={{ color: "#000" }}
            name="arrow-left"
            size={20}
            color="white"
          />
          <Text style={styles.pressableText}>Dashboard</Text>
        </Pressable>

        <View style={styles.headerText}>
          <Text style={styles.searchTitle}>Search Results for: "{search}"</Text>

          {/* ERROR TEXT IF THERE ARE NO RESULTS */}
          {datasource.length === 0 ? (
            <Text style={styles.errorText}>
              Sorry, it seems there are no results!
            </Text>
          ) : null}
        </View>

        {scrolled ? (
          <Pressable style={styles.scrollBtn} onPress={scrollToTop}>
            <Icon name="angle-double-up" size={50} color="#000" />
          </Pressable>
        ) : null}
      </View>

      {loading ? (
        <ActivityIndicator
          size="large"
          color="#48BBEC"
          style={{
            position: "absolute",
            top: height * 0.45,
            right: width * 0.45,
          }}
        />
      ) : (
        <FlatList
          contentContainerStyle={styles.flatlist}
          ref={flatlistRef}
          data={datasource}
          renderItem={renderItem}
          keyExtractor={(item) => makeID(8)}
          extraData={selectedId}
          numColumns={1}
          getItemLayout={(data, index) => ({
            index,
            length: 140,
            offset: 140 * index,
          })}
          viewabilityConfigCallbackPairs={
            viewabilityConfigCallbackPairs.current
          }
          viewabilityConfig={{
            itemVisiblePercentThreshold: 50,
          }}
          initialScrollIndex={index ? index : 0}
          ListFooterComponent={renderFooter}
        />
      )}
    </View>
  );
};

export default Search;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    // backgroundColor: "#582860",
  },
  back: {
    position: "absolute",
    left: 10,
    width: 140,
    borderRadius: 15,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
  },
  btnText: {
    color: "#000",
    fontSize: 18,
    fontWeight: "bold",
    textAlign: "center",
    fontFamily: "Avenir",
  },

  flatList: {
    marginTop: 20,
  },
  footer: {
    padding: 10,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
  },
  header: {
    position: "relative",
    marginTop: 20,
    width: width,
    height: 60,
    flexDirection: "column",
  },
  searchTitle: {
    position: "absolute",
    top: 30,
    color: "#000",
    fontSize: 24,
    fontFamily: myFont,
    textAlign: "center",
    width: "100%",
  },
  errorText: {
    position: "absolute",
    top: height * 0.4,
    fontSize: 24,
    color: "#000",
    textAlign: "center",
    width: "100%",
  },
  text: {
    fontSize: 18,
    fontFamily: myFont,
  },
  loadMoreBtn: {
    height: 40,
    padding: 10,
    borderColor: "#ccc",
    borderWidth: 1,
    borderRadius: 10,
    borderRadius: 4,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  pressableText: {
    color: "#000",
    marginLeft: 5,
    fontSize: 24,
    fontStyle: "italic",
    fontFamily: myFont,
  },
  scrollBtn: {
    position: "absolute",
    right: 20,
  },
});
